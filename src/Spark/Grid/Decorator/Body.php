<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;
use Spark\Grid\Column\Service;

class Body  extends Decorator
{
    public function render($content)
    {
        $rows = $this->getGrid()->loadData();   
        $columns = $this->getGrid()->getColumns();

        $summ = array();
        
        $rowCallback = $this->getGrid()->getRowClassCallback();
        $cellCallback = $this->getGrid()->getCellCallback();
       
        foreach ($rows as $row) {
            $rowClass = 'b-table_item';

            if ($rowCallback != null) {
                $rowClass .= ' ' . $rowCallback($row);
            }

            $content .= '<tr class="' . $rowClass . '">';
        
            foreach ($columns as $column) { 
                $column->setCurrentRow($row);
                $colValue = null;
                $columnContent = null;
                $columnName = $column->getColumn();

                if ($colValue == null && isset($row[$columnName])) {
                    $colValue = $row[$columnName];
                }
                  
                // Cell callback
                if ($cellCallback != null) {
                    $columnContent = $cellCallback($row, $columnName);
                }
                
                // Render
                if ($columnContent == null) {
                    $columnContent = $column->render(($column instanceof Spark\Grid\Column\Service) ? '' : $colValue);
                }
                
                // Summation
                // ToDo  надо переделать , чтобы бралось еще по всем данным , а не только тем которые на странице
                if ($column->getOption("summ")) {
                    if ($column->getOption("summContent")) {
                        @$summ[$columnName] = @$summ[$columnName] + $columnContent;
                    } else {
                        @$summ[$columnName] = @$summ[$columnName] + $colValue;
                    }
                }
                
                $class = $column->getOption("class");
                $content .= '<td class="' . $class . '" style="vertical-align: middle;">' . $columnContent . '</td>';
            } 
            $content .= '</tr>';
        }
        $this->getGrid()->renderedRows = sizeof($rows);
        $this->getGrid()->setSummationData($summ);
		

        if (sizeof($rows) == 0) { // Empty grid
            $content .= '<tr><td colspan="' . sizeof($columns) . '" class="grid-empty-alert">Таблица пуста</td></tr>' . PHP_EOL;
        }
        return $content;

    }
}