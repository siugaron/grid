<?php

namespace Spark\Grid\Column;

use Spark\Grid\Column;

class AjaxToggler extends Ajax\Ajax
{

    public function render($value)
    {
        if (!preg_match('/m-center/', $this->getOption("class"))) {
            $this->appendOption("class", ' m-center');
        }

        $class = $this->getGrid()->getClass();
        if(empty($class)) {
        	$class = 'light';
        }

        $output = '<div class="switcher js-switch switcher-theme-modern switcher-'.$class.' '.(($value)?"checked":"").' switcher-sm" data-url="'.$this->getDataUrl().'">
        				<input type="checkbox" data-class="switcher-info" '.(($value)?"checked":"").' name="' . $this->getName() . '">
        				<div class="switcher-toggler"></div>
        				<div class="switcher-inner"><div class="switcher-state-on">ON</div><div class="switcher-state-off">OFF</div></div></div>';
        return $output;
    }

}
