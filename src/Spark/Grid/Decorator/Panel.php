<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;

class Panel extends Decorator
{
    public function render($content)
    {   
        $label = $this->getGrid()->getPanelLabel();
        if (empty($label)) {
            return $content;
        }
        $controls = $this->getGrid()->getPanelControls();
        $modal = '';
        die('!');
        // Filters
        if ($this->getGrid()->hasFilters()) {
            $controls .= ' <a href="" class="btn btn-xs btn-primary btn-outline" data-toggle="modal" data-target="#' . $this->getGrid()->getId() . '-filters">Фильтры </a>';

            $form = '';
            foreach ($this->getGrid()->getFilters() as $name => $filter) {
                $form .= '<div class="form-group no-margin-hr">
                                    <label class="control-label">' . $filter->getOption('label') . '</label>
                                    ' . $filter->render() . '
                                </div>';
            }
            
            $modal = '<div id="' . $this->getGrid()->getId() . '-filters" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
                    <form action="" method="GET">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="">Фильтры</h4>
                                </div>
                                <div class="modal-body">
                                    ' . $form . '
                                </div> <!-- / .modal-body -->
                                <div class="modal-footer">
                                    <a class="btn btn-default" href="?">Сбросить</a>
                                    <!--button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button-->
                                    <button type="submit" class="btn btn-primary">Применить</button>
                                    
                                </div>
                            </div> <!-- / .modal-content -->
                        </div> <!-- / .modal-dialog -->
                    </form>
                </div>';
        }   



        //
        return '<div class="panel">
    <div class="panel-heading">
        <span class="panel-title">' . $label . '</span>
        <div class="panel-heading-controls">
            ' . $controls . '
        </div>
    </div>
    <div class="panel-body">' . $content . '</div>' . $modal . '</div>';
    }

    protected function _generateFormAction() {
        $gridid = $this->getGrid()->getId();
        $query = $_GET;
        if (!isset($query[$gridid])) {
            $query[$gridid] = array();
        }
        $url = http_build_query($query);
        return '?' . $url;
    }
					

}