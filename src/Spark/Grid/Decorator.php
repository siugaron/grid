<?php

namespace Spark\Grid;

use Spark\Grid\Grid as Grid;

class Decorator
{
    /**
     * @var Spark\Grid
     */
    protected $_grid;

    /**
     * @param Spark\Grid $grid
     * @return void
     */
    public function setGrid(Grid $grid)
    {
        $this->_grid = $grid;
    }

    /**
     * @return Spark\Grid
     */
    public function getGrid()
    {
        return $this->_grid;
    }

    public function render($content)
    {
        return $content;
    }
}
