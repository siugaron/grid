<?php
namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;
use Spark\Grid\Column;


class TableHeader extends Decorator
{
    public function render($content)
    {
       $tcontent = '<thead><tr>';
        $columns = $this->getGrid()->getColumns();
        foreach ($columns as $column) {
            $tcontent .= '<th class=" ' . $column->getOption('class') . '">' . ($column->isSortable()
                    ? '<a href="' . $this->_generateColumnLink($column) . '">'
                    : '') . $column->getLabel() . ($column->isSortable() ? '</a>' : '') . '</th>';
        }
        $tcontent .= '</tr></thead>';
        $tcontent .=  $content; 
        return $tcontent;
    }

    protected function _generateColumnLink(Column $column)
    {
        $gridid = $this->getGrid()->getId();
        $query = $_GET;

        if (!isset($query[$gridid])) {
            $query[$gridid] = array();
        }
        $query[$gridid]['sortby'] = $column->getName();
        $query[$gridid]['sortdir'] = ($column->isSorted() == "DESC")?'ASC':'DESC';
        $query[$gridid]['page'] = 1;
        $query[$gridid]['perpage'] = $this->getGrid()->getPerpage();
        $url = http_build_query($query);
        return '?' . $url;
    }
}
