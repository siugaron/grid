(function ($) {
    var GridSort = function ($el, url) {
        this.$el = $el;
        this.url = url;
        this.collection = {};
        this.initialize();
    };

    GridSort.prototype = {
        fixHelper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        update: function (id, prevId) {
            if (this.url) {
                var data = {
                    row_id: id,
                    prev_id: prevId
                };
                $.ajax({
                    type: 'GET',
                    data: data,
                    url: this.url
                });
            }

        },
        initialize: function ()  {
            this.$el.sortable({
                helper: this.fixHelper,
                axis: 'y',
                cursor: 'move'
            }).disableSelection();
            this.bindEvents();
        },
        getElId: function ($el) {
            return parseInt($el.find('.sort-id').data('id'))
        },

        bindEvents: function () {
            var that = this;
            this.$el.on( "sortupdate", function( event, ui ) {
                var prev = ui.item.prev(),
                    prevId = (prev.length > 0)? that.getElId(prev) : 1,
                    id = that.getElId(ui.item);
                that.update(id, prevId);
            });
        }
    };
    $('.grid-content tbody').each(function () {
        var url = $(this).parent().data('orderUrl');
        if (url) {
            var gridsort = new GridSort($(this), url);
        }

    });
})(jQuery);