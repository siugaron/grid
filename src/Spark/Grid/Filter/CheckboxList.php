<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;

class CheckboxList extends Options {

	public function init() {
        $this->_options['strict'] = true;
    }

    public function render() {
    	$gridid = $this->getGrid()->getId();
    	$options = $this->getMultiOptions();

    	$values = $this->getValue();
        if (!is_array($values)) {
            $values = array();
        }

    	$content = '<div class="row">
						<div class="col-xs-4">
							';

					$c = 0;
					foreach ($options as $key => $value) {
						$content.='
							<div class="checkbox">
								<label>
									<input type="checkbox" name="' . $gridid . '[filters][' . $this->getName() . '][]" value="' . $key . '" ' . ((in_array($key,$values)) ? 'checked' : '') . '>' . $value . '
								</label>
							</div>
						';
						$c++;
					}

					$content.='
						</div>
					</div>';
    	return $content;
    }
}