<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;

class Filters extends Decorator
{
    public function render($content) {
        $filters = $this->getGrid()->getFilters();
        if (sizeof($filters) == 0) {
            return $content;
        }
        // line 27 ->  - class = in
        $fcontent = PHP_EOL . '
			<header class="grid-filter">
                        <div id="accordion" class="panel-group">
                            <div class="panel">
                                <div class="panel-heading m-left-position">
									<span class="accordion-toggle  collapsed" data-toggle="collapse" data-parent="#accordion-example" href="#collapseOne">
										Фильтры
									</span>
								</div>

                                <div id="collapseOne" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">

                                        <form action="' . $this->_generateFormAction() . '" method="get"><div class="row">';


        foreach ($filters as $filter) {
            $fcontent .= '<div class="form-group col-sm-2">';
            if ($filter->getLabel() != '') {
                $fcontent .= '<label for="" class="control-label">' . $filter->getLabel() . '</label>';
            }

            $fcontent .= $filter->render() . '</div> ';
        }

        $fcontent .= '                                  </div>
                                                    <div class="form-actions m-1">
                                                        <button type="submit" class="btn btn-'.$this->getGrid()->getClass().'">Искать</button>
                                                        <a type="reset" class="btn" href="?">Сбросить</a>
                                                    </div>

                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div>


                    </header>
        ';
        return $fcontent . $content;
    }

    protected function _generateFormAction() {
        $gridid = $this->getGrid()->getId();
        $query = $_GET;
        if (!isset($query[$gridid])) {
            $query[$gridid] = array();
        }
        $url = http_build_query($query);
        return '?' . $url;
    }

    protected function _generateFormHidden() {
        $query = $_GET;
        $gridid = $this->getGrid()->getId();
        unset($query[$gridid]);
        $output = '';
        foreach ($query as $name=>$value) {
            if (is_array($value)) {
                //Not implemented
            } else {
                $output .= '<input type="hidden" name="' . e($name) . '" value="' . e($value) . '"/>';
            }
        }
        return $output;
    }
}

