<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;

class Options extends Filter {

	protected $multiOptions;
	/**
     * Retrieve options array
     *
     * @return array
     */
    protected function _getMultiOptions()
    {
        if (null === $this->multiOptions || !is_array($this->multiOptions)) {
            $this->multiOptions = array();
        }

        return $this->multiOptions;
    }

    /**
     * Add an option
     *
     * @param  string $option
     * @param  string $value
     * @return Spark\Grid\Filter\Options
     */
    public function addMultiOption($option, $value = '')
    {
        $option  = (string) $option;
        $this->_getMultiOptions();
        $this->multiOptions[$option] = $value;
        return $this;
    }

    /**
     * Add many options at once
     *
     * @param  array $options
     * @return Spark\Grid\Filter\Options
     */
    public function addMultiOptions(array $options)
    {
        foreach ($options as $option => $value) {
            if (is_array($value)
                && array_key_exists('key', $value)
                && array_key_exists('value', $value)
            ) {
                $this->addMultiOption($value['key'], $value['value']);
            } else {
                $this->addMultiOption($option, $value);
            }
        }
        return $this;
    }

    /**
     * Set all options at once (overwrites)
     *
     * @param  array $options
     * @return Spark\Grid\Filter\Options
     */
    public function setMultiOptions(array $options)
    {
        $this->clearMultiOptions();
        return $this->addMultiOptions($options);
    }

    /**
     * Retrieve single multi option
     *
     * @param  string $option
     * @return mixed
     */
    public function getMultiOption($option)
    {
        $option  = (string) $option;
        $this->_getMultiOptions();
        if (isset($this->multiOptions[$option])) {
            return $this->multiOptions[$option];
        }

        return null;
    }

    /**
     * Retrieve options
     *
     * @return array
     */
    public function getMultiOptions()
    {
        $this->_getMultiOptions();
        return $this->multiOptions;
    }

    /**
     * Remove a single multi option
     *
     * @param  string $option
     * @return bool
     */
    public function removeMultiOption($option)
    {
        $option  = (string) $option;
        $this->_getMultiOptions();
        if (isset($this->multiOptions[$option])) {
            unset($this->multiOptions[$option]);
            return true;
        }

        return false;
    }

    /**
     * Clear all options
     *
     * @return Spark\Grid\Filter\Options
     */
    public function clearMultiOptions()
    {
        $this->multiOptions = array();
        return $this;
    }


}