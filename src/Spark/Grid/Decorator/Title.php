<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;
use Spark\Grid\Column\Service;

class Title extends Decorator
{
    public function render($content)
    {   
        $paginator = $this->getGrid()->getPaginator();

        $title = $this->getGrid()->getTitle();
        $actions = $this->getGrid()->getActions();
        $tcontent = "";
        $tcontent .= ' <div class="box-header table-header clearfix">';
        if($title!==null&&$title!==false) {
            $tcontent .= '<h2>'.$title.'</h2>';
        }
        /*
        
             
             $paginatorContent .= '<div class="col-xs-5"><div class="btn-group">';
             foreach ($pages as $b) {
                 $paginatorContent .= '<a class="btn btn-default' . (($b == $paginator->getCurrentPage()) ? ' active' : '') . '" href="' . $this->_generatePerPageLink($b) . '">' . $b . '</a><br>';
             }
             $paginatorContent .= '</div></div>';
         } */
         
        if ($this->getGrid()->getShowPerPageSelector()) {
            $pages = array(10, 20, 50, 100);

            $tcontent .= '<div class="table_output">
                    <div class="DT-per-page DT-lf-right">
                        <div class="dataTables_length" id="jq-datatables-example_length"><label>Показывать по: </label>
                            <div class="btn-group">
                                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    ' . $paginator->perPage() . '
                                    <span class="caret"></span>
                                </a> 
                                <ul class="dropdown-menu" role="menu">' ;
                                
                                foreach ($pages as $b) {
                                     $tcontent .= '<li><a class="btn btn-default" href="' . $this->_generatePerPageLink($b) . '">' . $b . '</a></li>';
                                 }
                                
            $tcontent .= '</ul>
                            </div>
                        </div>
                    </div>';
        }

        if((strlen($title)>0)||sizeof($actions)>0) {
            $tcontent .= '
              
                    
                    <div class="box-icon DT-lf-right">
                        ';

                        foreach($actions as $action) {
                            $tcontent .=  $action->render();
                        }
                $tcontent .= '    </div>
                
            ';
        }
        $tcontent .= '
        </div>';
        $tcontent .= '</div>'; 
        return $tcontent . $content;
    }



    protected function _generatePerPageLink($perPage)
    {
        $gridid = $this->getGrid()->getId();
        $query = $_GET;
        if (!isset($query[$gridid])) {
            $query[$gridid] = array();
        }
        $query[$gridid]['perpage'] = $perPage;
        $query[$gridid]['page'] = 1;
        $url = http_build_query($query);
        return '?' . $url;
    }
/*

<a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
<a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
<a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>

*/
}