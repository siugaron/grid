<?php
namespace Spark\Grid\Column;

use Spark\Grid\Column;

class Callback extends Column {

    public function render($value)
    {
        $func = $this->getOption("callback");
        return $this->applyLink($func($this->getCurrentRow()));
    }
}