<?php
namespace Spark\Grid\Column;

use Spark\Grid\Column;

class Checkbox extends Column {
	
	public function render($value)
    {
    	$message = $this->getOption("message");
        return '<div class="checkbox" style="margin: 0;">
					<label>
						<input type="checkbox" name="id[' . $value . ']" value="1" class="px" form="'.$this->getOption('form').'"/>
						<span class="lbl">&nbsp; ' .$message. ' </span>
					</label>
				</div>'; 
        
    }

}