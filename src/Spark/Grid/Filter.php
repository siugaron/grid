<?php

namespace Spark\Grid;

use Spark\Grid\Source as Source;
use Spark\Grid\Source\Builder;
use Spark\Grid\Source\Collection;
//use Illuminate\Database\Eloquent\Builder;

class Filter {
    
    protected $_order;

    public function __construct($name, $options = array()) {
        $this->setName($name);
        $this->setOptions($options);
        $this->init();
    }

    public function initValue() {
        if (isset($_GET[$this->getGrid()->getId()]) && is_array($_GET[$this->getGrid()->getId()])) {
            //Параметры грида
            $params = $_GET[$this->getGrid()->getId()];
            if (isset($params['filters']) && isset($params['filters'][$this->getName()])) {
                $this->setValue($params['filters'][$this->getName()]);
            }
        }
    }

    public function init() {
        
    }

    /**
     * @var Spark\Grid
     */
    protected $_grid;

    /**
     * @param Spark\Grid $grid
     * @return void
     */
    public function setGrid(Grid $grid) {
        $this->_grid = $grid;
    }

    /**
     * @return Spark\Grid
     */
    public function getGrid() {
        return $this->_grid;
    }

    protected $_options = array();

    public function setOptions($options) {
        if (sizeof($options) > 0) {
            foreach ($options as $key => $value) {
                $method = "set" . ucfirst($key);
                if (method_exists($this, $method)) {
                    $this->$method($value);
                    unset($options[$key]);
                }
            }
        }
        $this->_options = $options;
        /* if (isset($options['value'])) {
          $this->setValue($options['value']);
          } */
    }

    public function getOptions() {
        return $this->_options;
    }

    public function getOption($name) {
        if (isset($this->_options[$name])) {
            return $this->_options[$name];
        }
        return null;
    }
    /**
     * @var string filter name
     */
    protected $_name;

    /**
     * @param string $name
     * @return void
     */
    public function setName($name = '') {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @var string filter value
     */
    protected $_value;

    /**
     * @param string $value
     * @return void
     */
    public function setValue($value = '') {
        $this->_value = $value;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->_value;
    }

    protected $width = 3;
    
    public function setWidth($value = '') {
        $this->width = $value;
    }

    public function getWidth() {
        return $this->width;
    }
    
    public function getLabel() {
        return @$this->_options['label'];
    }
    

    public function apply($source) { 
        $this->initValue();
        $options = $this->getOptions();
        $value = $this->getValue();
        if ($value === '' || $value === null) {
            return true;
        }
//dd($source);
        if ($source instanceof Builder) {
            $this->_applySelectConditions($source);
        } elseif ($source instanceof Collection) {
            $this->_applyCollectionConditions($source);
        } else {
            throw new \Exception('Неизвестный тип источника данных');
        } 
    } 

    protected function _applySelectConditions($source) { 
        $data = $source->getData(); 

        if ($source instanceof Builder) {

            $options = $this->getOptions();
            $value = $this->getValue();

            if (isset($options['strict']) && $options['strict'] == true) {
                // Точный поиск значения
                if (isset($options['sourceAnyValue']) && $options['sourceAnyValue'] == true) { 
                    if (!empty($value)) {
                       $data->having($this->getName(), '<>', '\'\' ');
                    }
                } else { 
                    if (isset($options['sourceUseHaving']) && $options['sourceUseHaving'] == true) {
                        $data->having($this->getName(), '=', $value); 
                    } else {  
                        $data->where($this->getName(),  '=', $value); 
                    }
                } 
            } else {
                // Поиск вхождения по тексту
                if (isset($options['sourceUseHaving']) && $options['sourceUseHaving'] == true) {
                    // die("!");
                    $data->having($this->getName(), 'LIKE', '%' . $value . '%');
                } else {
                    $data->where($this->getName(), 'LIKE', '%' . $value . '%');
                }
            }
        }
        //dd($data->toSql());
        $source->setData($data);
    }

    protected function _applyCollectionConditions($source) { 
        $data = $source->getData(); 

        if ($source instanceof Collection) {
            $options = $this->getOptions();
            $value = $this->getValue();

            $field_name = $this->getName();

            if(is_array($value)) {
                $data = $data->filter(function($row) use ($value, $field_name) {
                    if(in_array($row->$field_name, $value)) {
                        return true;
                    }
                });
            } else {
                // Точный поиск значения
                if (isset($options['strict']) && $options['strict'] == true) 
                {

                    $data = $data->filter(function($row) use ($value, $field_name) { 
                        if($row->$field_name == $value) 
                        {
                            return true;
                        }
                    });

                } else {

                    $data = $data->filter(function($row) use ($value, $field_name){
                        if(strpos($row->$field_name, $value)===0) 
                        { 
                            return true;
                        }
                    });

                }
            }
        }

        $source->setData($data);
    }

    public function render() {
        return '';
    }

    public function getOptionValue($name) {
        $options = $this->getOptions();
        if(is_array($options)&&isset($options[$name])) {
            return $options[$name];
        }
        return "";
    }


    public function setOrder($order)
    {
        $this->_order = (int) $order;
        return $this;
    }

    public function getOrder()
    {
        return $this->_order;
    }

}