<?php
/**
 * Источник данных на основе HasMany
 *
 * Используется когда источник данных Illuminate\Database\Eloquent\Relations\HasMany
 */

namespace Spark\Grid\Source;

use Spark\Grid;
use Spark\Grid\Source;

/**
 * Class HasMany
 * @package Spark\Grid\Source
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $_data
 */
class HasMany extends Source
{
    public function order($column, $dir = "ASC")
    {
        $this->_sortColumn = $column;
        $this->_sortDirection = strtoupper($dir);
       // $this->_data->reset(Select::ORDER)->order($column . ' ' . $dir);
    }
    protected $_sortColumn;
    protected $_sortDirection;

    public function getSortColumn() {
        return $this->_sortColumn;
    }
    public function getSortDirection() {
        return $this->_sortDirection;
    }

    public function isSortable() {
        return true;
    }

    public function count()
    {
        $this->_data->count();
    }

    public function initPaginatorAdapter($options = array())
    {
        return $this->_data->paginate($this->getPerPage());
    }

}