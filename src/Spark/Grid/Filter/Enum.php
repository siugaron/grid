<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;
// ToDo
class Enum extends Select {

	protected $_viewType;

    public function render() {
    	$options = $this->getOptions();
    	$gridid = $this->getGrid()->getId();

    	switch($this->getViewType()) {
    		case "checkboxlist":
    			$content = '<div class="row">
						<div class="col-xs-4">
							';

					$c = 0;
					foreach ($this->getMultiOptions() as $key => $value) {
						$content.='
							<div class="checkbox">
								<label>
									<input type="checkbox" name="' . $gridid . '[filters][' . $this->getName() . '][]" value="' . $key . '" ' . ((in_array($key,$values)) ? 'checked' : '') . '>' . $value . '
								</label>
							</div>
						';
						$c++;
					}

					$content.='
						</div>
					</div>';
    			break;
    		case "radio":
    			// ToDo
    			break;
    		case "select":
    		default:
    			$content = '
			    	<select name="' . $gridid . '[filters][' . $this->getName() . ']" class="' . (isset($options['class'])?$options['class']:'form-control') . '"><option value="">'. $this->getOptionValue('label'). '</option>';
				    	foreach ($this->getMultiOptions() as $key => $value) {
				    		$content .= '<option value="' . $key . '"' . ($key==$this->getValue()?'selected':'').'>' . $value . '</option>';
				    	}

			    	$content .= '</select>';
    			break;
    	}
    	
    	return $content;
    }

    public function setViewType($type = "select") {
    	$this->_viewType = $type;
    	return $this->_viewType;
    }

    public function getViewType() {
    	if($this->_viewType==null||$this->_viewType==false) {
    		$this->_viewType = $this->setViewType();
    	}
    	return $this->_viewType;
    }


}