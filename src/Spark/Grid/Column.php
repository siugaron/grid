<?php
/**
 * Класс колонки
 */
namespace Spark\Grid;



class Column
{   
    protected $_order = 0;
    

    public function __construct($name = '', $options = array())
    {
        $this->setName($name);
        $this->setOptions($options);
    }

    protected $_options = array();

    public function setOptions(array $options)
    {
        foreach ($options as $name => $value) {
            $method = "set" . ucfirst($name);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        $this->_options = $options;
    }

    public function setOption($name, $value)
    {
        $this->_options[$name] = $value;
    }

    public function getOption($name = '')
    {
        if (isset($this->_options[$name])) {
            return $this->_options[$name];
        }
        return null;
    }
    public function appendOption ($name, $value) {
        @$this->_options[$name] .= $value;
        return $this;
    }
    
    
    /**
     * @var Grid
     */
    protected $_grid;

    /**
     * @param Grid $grid
     *
     * @return $this
     */
    public function setGrid(Grid $grid)
    {
        $this->_grid = $grid;

        return $this;
    }

    /**
     * @return Grid
     */
    public function getGrid()
    {
        return $this->_grid;
    }

    protected $_value;

    public function setValue($value)
    {
        $this->_value = $value;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function render($value)
    {
        $this->setValue($value);
        return $this->applyLink(e($this->_value));
    }

    protected $_label;

    public function setLabel($label = '')
    {
        $this->_label = $label;
    }

    public function getLabel()
    {
        return $this->_label;
    }

    /**
     * @var string column name
     */
    protected $_name;

    /**
     * @param string $name
     * @return void
     */
    public function setName($name = '')
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    public function getColumn() {
        if (isset($this->_options['column'])) {
            return $this->_options['column'];
        } 
        return $this->_name;
    }
    
    
    public function isSortable()
    {
        if ($this->getOption('sortable') !== null) {
            return $this->getOption('sortable');
        }
        return false;
    }


    public function isSorted()
    {
        if ($this->isSortable() && $this->getGrid()->getParam('sortby') == $this->_name) {
            return strtoupper($this->getGrid()->getParam('sortdir'));
        }
        return false;
    }

    protected $_currentRow;
    public function setCurrentRow($data) {
        $this->_currentRow = $data;
    }
    public function getCurrentRow() {
        return $this->_currentRow;
    }
    
    protected function applyLink ($content, $options = array()) {
        $link = $this->getOption("link");
        if (!is_array($link)) {
            return $content;
        }

        $method = @$options["method"]; 
        $title = @$link["title"];
        $target = @$link["target"];


        $class = @$link['class'];
        if (!empty($class)) {
            $class = ' class="' . $class . '"';
        }

        if (!empty($title)) {
            $cr = $this->getCurrentRow();
            preg_match('/{(.*)}/iu', $title, $match);
            if(isset($match[1])){
                if(isset($cr[$match[1]])){
                    $title = preg_replace('/{(.*)}/iu', $cr[$match[1]], $title);
                }
            }
            $title = ' title=\'' . $title . '\'';
        }
        if (!empty($target)) {
            $target = ' target="' . $target . '"';
        }

        $link = $this->createLink($options);
        return '<a href="' . $link . '"' . $class . ''. $title . $target . ' data-method= ' . $method . ' data-token="'.csrf_token().'">' . $content . '</a>';
    }

    protected function createLink ($options = array())
    {
        $link = $this->getOption("link");

        if (!is_array($link)) {
            return $link;
        }

        $route = @$link["route"];
        $params = @$link["params"]; 
        $get = @$link["get"];

        if (!is_array($params)) {
            $params = array();
        }
        if (!is_array($get)) {
            $get = array();
        }

        foreach ($params as $param => $value) {
            $match = array();
            preg_match('/{(.*)}/iu', $value, $match);
            if (!isset($match[1])) {
                continue;
            }
            $cr = $this->getCurrentRow();
            if (is_array($cr) && isset($cr[$match[1]])) {
                $params[$param] = $cr[$match[1]];
            } else {
                if (isset($cr->{$match[1]})) {
                    $params[$param] = $cr->{$match[1]};
                }
            }
        }

        foreach ($get as $param => $value) {
            $match = array();
            preg_match('/{(.*)}/iu', $value, $match);
            if (!isset($match[1])) {
                continue;
            }
            $cr = $this->getCurrentRow();
            if (is_array($cr) && isset($cr[$match[1]])) {
                $params[$param] = $cr[$match[1]];
            } else {
                if (isset($cr->{$match[1]})) {
                    $params[$param] = $cr->{$match[1]};
                }
            }
        }

        if (strpos($route, '@') !== false) { //Controller@action link
            return action($route, $params);
        } else {
            return route($route, $params);
        }
    }

    /**
     * @param $order
     *
     * @return $this
     */
    public function setOrder($order)
    {
        $this->_order = (int) $order;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Проверка, является ли сущность базовой
     *
     * @return boolean
     */
    public function getBaseStatus() {
        if(is_object($this->getCurrentRow())) {
            if($this->getCurrentRow()->is_base === 1) {
                return true;
            }
        }
        return false;
    }

    
}