<?php
// ToDo
namespace Spark\Grid\Column\Ajax;

use Spark\Grid\Column;

class Ajax extends Column {
	
	public function getDataUrl() {
        $link = $this->getOption("action");
        if (!is_array($link)) {
            return "";
        }
        $route = @$link["route"];
        $params = @$link["params"];
        if (!is_array($params)) {
            $params = array();
        }
        foreach ($params as $param => $value) {
            $match = array();
            preg_match('/{(.*)}/iu', $value, $match);
            if (!isset($match[1])) {
                continue;
            }
            $cr = $this->getCurrentRow();
            
            if (isset($cr->{$match[1]}) || (is_array($cr) && isset($cr[$match[1]]))) {
                if (is_array($cr)) {
                    $params[$param] = $cr[$match[1]];
                } else {
                    $params[$param] = $cr->{$match[1]};
                }
            }
        }
        $link = $this->getGrid()->createUrl($route, $params);
        return $link;
    }
}