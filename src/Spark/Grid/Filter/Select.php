<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;

class Select extends Options {

    public function render() {
    	$options = $this->getOptions();
    	$gridid = $this->getGrid()->getId();
    	$content = '
    	<select name="' . $gridid . '[filters][' . $this->getName() . ']" class="' . (isset($options['class'])?$options['class']:'form-control') . '"><option value="">'. $this->getOptionValue('label'). '</option>';
	    	foreach ($this->getMultiOptions() as $key => $value) {
	    		$content .= '<option value="' . $key . '"' . (($key===$this->getValue())||((string)$key===(string)$this->getValue())?'selected':'').'>' . $value . '</option>';
	    	}

    	$content .= '</select>';
    	return $content;
    }
}