<?php
namespace Spark\Grid\Column;

use Spark\Grid\Column;

class Enum extends Column {
	
	protected $_multiOptions;

    /**
     * Retrieve options array
     * @return array
     */
    protected function _getMultiOptions() {
        if (null === $this->_multiOptions || !is_array($this->_multiOptions)) {
            $this->_multiOptions = array();
        }

        return $this->_multiOptions;
    }

    /**
     * Add an option
     *
     * @param  string $option
     * @param  string $value
     * @return Spark\Grid\Column\Enum
     */
    public function addMultiOption($option, $value = '') {
        $option = (string) $option;
        $this->_getMultiOptions();
        $this->_multiOptions[$option] = $value;
        return $this;
    }

    /**
     * Add many options at once
     *
     * @param  array $options
     * @return Spark\Grid\Column\Enum
     */
    public function addMultiOptions(array $options) {
        foreach ($options as $option => $value) {
            if (is_array($value)
                    && array_key_exists('key', $value)
                    && array_key_exists('value', $value)
            ) {
                $this->addMultiOption($value['key'], $value['value']);
            } else {
                $this->addMultiOption($option, $value);
            }
        }
        return $this;
    }

    /**
     * Set all options at once (overwrites)
     *
     * @param  array $options
     * @return Spark\Grid\Column\Enum
     */
    public function setMultiOptions(array $options) {
        $this->clearMultiOptions();
        return $this->addMultiOptions($options);
    }

    /**
     * Retrieve single multi option
     *
     * @param  string $option
     * @return mixed
     */
    public function getMultiOption($option) {
        $option = (string) $option;
        $this->_getMultiOptions();
        if (isset($this->_multiOptions[$option])) {
            return $this->_multiOptions[$option];
        }

        return null;
    }

    /**
     * Retrieve options
     *
     * @return array
     */
    public function getMultiOptions() {
        $this->_getMultiOptions();
        return $this->_multiOptions;
    }

    /**
     * Remove a single multi option
     *
     * @param  string $option
     * @return bool
     */
    public function removeMultiOption($option) {
        $option = (string) $option;
        $this->_getMultiOptions();
        if (isset($this->_multiOptions[$option])) {
            unset($this->_multiOptions[$option]);
            return true;
        }

        return false;
    }

    /**
     * Clear all options
     *
     * @return Spark\Grid\Column\Enum
     */
    public function clearMultiOptions() {
        $this->_multiOptions = array();
        return $this;
    }

    public function render($value) {
        if (isset($this->_multiOptions[$value])) {
            return $this->applyLink(e($this->_multiOptions[$value]));
        }
        return $this->applyLink(e($value));
    }
}