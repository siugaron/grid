<?php
/**
 * Абстрактный источник данных
 */
namespace Spark\Grid;

class Source
{
    protected $_data;
    protected $_options;

    public function __construct($data, $options = array())
    {
        $this->setData($data);
        $this->setOptions($options);
    }

    protected function sortData($data)
    {
        return false;
    }

    public function setData($data)
    {
        $this->_data = $data;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function setOptions($options)
    {
        $this->_options = $options;
    }

    public function getOptions()
    {
        return $this->_options;
    }

    public function order($column, $dir = "ASC")
    {
        return false;
    }

    public function filter($column, $pattern, $options = array())
    {
        return false;
    }

    public function getSortColumn()
    {
        return false;
    }

    public function getSortDirection()
    {
        return false;
    }

    public function isSortable()
    {
        return false;
    }

    public function isSorted()
    {
        return false;
    }

    public function isUpdateable()
    {
        return false;
    }

    public function count()
    {
        return 0;
    }

    public function initPaginatorAdapter()
    {
        return new Null();
    }

    protected $_grid;

    public function setGrid(Grid $grid) {
        $this->_grid = $grid;
        return $this;
    }

    public function getGrid() {
        return $this->_grid;
    }

    public function getPerPage() {
        return $this->_grid->getPerPage();
    }
}