<?php
// ToDo

namespace Spark\Grid\Column;

use Spark\Grid\Column;

class Image extends Column {
	
	public function render($value) {
		$content = '
			<img src="'.$this->getSrc($value).'" alt="" class="miniature" />
		';

		return $content;
	}

	public function getSrc($value) {
        $width = $this->getOption('width') !== null?$this->getOption('width '):50;
        $height = $this->getOption('height') !== null?$this->getOption('height '):50;
        $type = $this->getOption('type') !== null?$this->getOption('type'):'fit';

		return spark_asset_image($value, $width, $height, $type);
	}
}