<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;

class TableBody  extends Decorator
{
    public function render($content)
    {
        $content = '<tbody>' . PHP_EOL . $content . '</tbody>' . PHP_EOL;
        return $content;
    }
}