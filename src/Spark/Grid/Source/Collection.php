<?php
/**
 * Источник данных на основе Collection
 */

namespace Spark\Grid\Source;

use Spark\Grid;
use Spark\Grid\Source;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
// @ToDo

/**
 * Class Collection
 * @package Spark\Grid\Source
 *
 * @property \Illuminate\Support\Collection $_data
 */
class Collection extends Source
{
    public function order($column, $dir = "ASC")
    {
        $this->_sortColumn = $column;
        $this->_sortDirection = strtoupper($dir);
       // $this->_data->reset(Select::ORDER)->order($column . ' ' . $dir);
    }
    protected $_sortColumn;
    protected $_sortDirection;

    public function getSortColumn() {
        return $this->_sortColumn;
    }
    public function getSortDirection() {
        return $this->_sortDirection;
    }

    public function isSortable() {
        return true;
    }

    public function count()
    {
        return $this->_data->count();
    }

    public function initPaginatorAdapter()
    {   
        // @
        $data = $this->_data->toArray();
        return new Paginator($this->_data, count($data), $this->getPerPage());
        //return $data->paginate($this->getPerPage());
    }

}