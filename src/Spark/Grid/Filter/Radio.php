<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;

class Radio extends Options {

	public function init() {
		$this->_options['strict'] = true;
	}

    public function render() {
    	$gridid = $this->getGrid()->getId();
    	$options = $this->getMultiOptions();

    	$radio_value = $this->getValue();

    	$content = '<div class="controls">';
        $content.='<div class="radio">
          <label>
            <input type="radio" name="' . $gridid . '[filters][' . $this->getName() . ']" value="" checked>
            Не важно
          </label>
        </div>';
    	foreach ($options as $key => $value) {
    		$content.='<div class="radio">
						  <label>
						    <input type="radio" name="' . $gridid . '[filters][' . $this->getName() . ']" value="' . $key . '" '. (($key===$radio_value) ? 'checked' : '') . '>
						    '.$value.'
						  </label>
						</div>';
    	}
		$content.='</div>';
    	return $content;
    }
}