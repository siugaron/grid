<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;
use Spark\Grid\Column\Service;

class Batch extends Decorator
{
    public function render($content)
    {
        $actions = $this->getGrid()->getBatchActions();
        
        if (sizeof($actions) < 1 || $this->getGrid()->renderedRows == 0) {
            return $content;
        }
        
        $class = $this->getGrid()->getClass();
        if(empty($class)) {
            $class = 'light';
        }

        $tcontent = $content;
        $tcontent .= '<div class="well"><form action="" method="POST" class="form-inline js-form-batch" id="batch-form">';
        $tcontent .= '
            <div class="form-group">
                <select class="form-control js-form-batch-select">';
                    foreach ($actions as $name => $url) {
                        $tcontent .= '<option value="' . $url . '">' . $name . '</option>';
                    } 
        $tcontent .= '</select>
            </div>
            
                <button type="submit" class="btn btn-'.$class.' js-form-batch-submit">Применить</button>';

        $tcontent .= '</form></div>';
        return $tcontent;
    }

}
