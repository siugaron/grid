<?php

namespace Spark\Grid\Column;

use Spark\Grid\Column;

class Sort extends Column {

    public function render($value) {
        $this->setOption('class', 'm-grid-sort-hander');
        return '<span class="sort-id" data-id="' . $value . '"><img src="/assets/images/content/sortable.png" alt="Элементы сортироются"></span>';
    }

}