<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;

class Text extends Filter {

    public function render() {
    	$options = $this->getOptions();
    	$gridid = $this->getGrid()->getId();
    	return '<input type="text" name="' . $gridid . '[filters][' . $this->getName() . ']" class="' . (isset($options['class'])?$options['class']:'form-control') . '"' . (isset($options['label'])?'placeholder="'.$options['label'] . '"':'') . ' value="' . $this->getValue() . '">';
    }
}


