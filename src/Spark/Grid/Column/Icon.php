<?php
namespace Spark\Grid\Column;

use Spark\Grid\Column;

// ToDo
class Icon extends Column
{
    
    public function render($value)
    {
        $icon = $this->getOption("icon");
        $method = $this->getOption("method");
        $class = '' ; //$this->getOption("class");
        $message = $this->getOption("message");

        $is_base = $this->getBaseStatus();
      
        if($method === 'delete') {
            if(!empty($message)) {
                $data_mess = "data-message='".$message."'";
            } else {
                $data_mess = "";
            }
            if($is_base) {
                return "";
            }
        	return '<form method="POST" action="'.$this->createLink().'" name="delete" style="margin: 0;">
        	<input name="_method" type="hidden" value="DELETE" >
        	<input name="_token" type="hidden" value="'.csrf_token().'">
        	<button class="btn btn-xs btn-'.$this->getGrid()->getClass().' '. $class .'" '.$data_mess .'><span class="btn-label icon fa fa-'.$icon.'"></span></button>
			</form> ';
        }

 		$icon = '<i class="fa fa-'.$icon.'"></i>';

        return $this->applyLink($icon, array('method'=>$method));
        
    }
}