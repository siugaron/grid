<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;

class Paginator extends Decorator
{
    public function render($content)
    {   
        $rows = $this->getGrid()->loadData();  
        if (sizeof($rows) == 0) {
            return $content;
        }

        $class = $this->getGrid()->getClass();
        if(empty($class)) {
            $class = 'no-bg';
        } else {
            $class = 'bg-'.$class;
        }

        $paginator = $this->getGrid()->getPaginator();
        

        $paginatorContent = '<div class="table-footer">';
        if ($this->getGrid()->getShowPerPageSelector()) {
            $pages = array(10, 20, 50, 100);
            $paginatorContent .= '<div class="btn-group pull-right">';

            foreach ($pages as $b) {
                if ($paginator->perPage() == $b) {
                    $paginatorContent .= '<a class="btn btn-primary disabled" href="' . $this->getGrid()->link(['perpage'=>$b, 'page'=>1]) . '">' . $b . '</a> ';    
                } else {
                    $paginatorContent .= '<a class="btn btn-outline" href="' . $this->getGrid()->link(['perpage'=>$b, 'page'=>1]) . '">' . $b . '</a> ';
                }
                
            }
                
            $paginatorContent .= '</div>';
        }
        /*if ($paginator->lastPage() > 1) {
                $paginatorContent .= '<div class="pagination-wrap ' . $class . '" > <ul class="pagination">';
                $currPage = $paginator->currentPage();
                if ($paginator->currentPage() > 1) {
                $paginatorContent .= '<a href="http://tm4.lab.devspark.ru/computers/?list%255Bperpage%255D=10&amp;list%255Bpage%255D=1&amp;page='.--$currPage.'" rel="prev">«</a>';
                }  else {
                    $paginatorContent .= '<li class="disabled"><span>«</span></li>';
                }
            if ($paginator->lastPage() < 12) {
                 for ($i=1; $i<12; $i++) {
                    if ($i == $currPage){
                        $paginatorContent .='<li><span>'.$i.'</span></li>';
                    } else {
                       $paginatorContent .= '<li><a href="http://tm4.lab.devspark.ru/computers/?list%25255Bperpage%25255D=10&amp;list%25255Bpage%25255D=1&amp;list%5Bperpage%5D=20&amp;list%5Bpage%5D=1&amp;page='.$i.'">'.$i.'</a></li>'; 
                    }
                }
            }

            // dd($paginator->appends($_GET));
            // $current = $paginator->getCurrentPage();
            // $this->getGrid()->link(['page'=>1])



            // $paginatorContent .= $paginator->appends($_GET)->render();
            $paginatorContent .= '</ul>';
            $paginatorContent .= '';
            $paginatorContent .= '</div>'; 
            $paginatorContent .= '';
        }*/

        $paginatorContent .= '<ul class="pagination">';
        $current = $this->getGrid()->getPage();
        
        //
        //dd($paginator);
        

        $total = $paginator->lastPage();
        $pagesToShow = 5;
        $grid = $this->getGrid(); 

        $renderPageItem = function ($num) use ($current, $grid) {
            if ($num == $current) {
                return '<li class="active"><a href="' . $grid->link(['page'=>$num]) . '">' . $num . '</a></li>';;
            } else {
                return '<li class=""><a href="' . $grid->link(['page'=>$num]) . '">' . $num . '</a></li>';;
            }
        };

        // 1

        if ($current > 1) {
            $paginatorContent .= '<li class=""><a href="' . $grid->link(['page'=>$current-1]) . '">&laquo;</a></li>';
        } else {
            $paginatorContent .= '<li class="disabled"><a href="">&laquo;</a></li>';
        }

        // 2
        if ($current > 3) {
            $paginatorContent .= $renderPageItem(1);
            if ($current > 4) {
                $paginatorContent .= $renderPageItem(2);
            }
            if ($current >= 6) {
                $paginatorContent .= '<li class="disabled"><a href="">...</a></li>';
            }
        }

        // 3

        $from = $current - 2;
        $to = $current + 2;
        if ($from < 1) {
            $from = 1;
            $to = 5;
        }

        if ($to > $total) {
            $to = $total;
            $from = $total-5;
            if ($from < 1) {
                $from = 1;
            }
        }

        for ($i = $from; $i<= $to; $i++) {
            $paginatorContent .= $renderPageItem($i);
        }

        // 4
        if ($to+3 >= $total) {
            for ($i = $to+1; $i<= $total; $i++) {
                $paginatorContent .= $renderPageItem($i);
            }
        } else {
            $paginatorContent .= '<li class="disabled"><a href="">...</a></li>';
            $paginatorContent .= $renderPageItem($total-1);
            $paginatorContent .= $renderPageItem($total);
        }

        // 5

        if ($current != $total) {
            $paginatorContent .= '<li class=""><a href="' . $grid->link(['page'=>$current+1]) . '">&raquo;</a></li>';
        } else {
            $paginatorContent .= '<li class="disabled"><a href="">&raquo;</a></li>';
        }

        $paginatorContent .= '</ul>';
        //$paginatorContent .= $paginator->appends($_GET)->render();
        $paginatorContent .= '</div>';
        return $content . $paginatorContent;
    }

					

}