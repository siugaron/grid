<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;
use Spark\Grid\Source;

class Checkbox extends Filter {
	// Class => switch-primary / switch-success / switch-warning  1

    public function render() {
    	$options = $this->getOptions();
    	$gridid = $this->getGrid()->getId();
    	$checked = '';
    	if (@isset($_GET[$gridid]['filters'][$this->getName()]) && $this->getValue() == $this->getCheckedValue()) {
    		$checked = 'checked';
    	}

    	$content = '<div class="row">
			<div class="controls">
					<label class="switch '.$this->getOptionValue("class").'">
				      <input type="checkbox" class="switch-input" value="' . $this->getUncheckedValue() . '" name="' . $gridid . '[filters][' . $this->getName() . ']" '.$checked.'>
				      <span class="switch-label" data-on="'.(isset($options['on'])?$options['on']:'On').'" data-off="'.(isset($options['of'])?$options['of']:'Of').'"></span>
				      <span class="switch-handle"></span>
				    </label>
			</div>
		</div>';
    	return $content;
    }

    public function getCheckedValue() {
        return "1";
    }
    public function getUncheckedValue() {
        return "0";
    }

	public function init() {
		$this->_options['strict'] = true;
	}

    public function apply(Source $source) {
        $this->initValue();
        if ($this->getValue() == $this->getUncheckedValue() || $this->getValue() == '') {
            return true;
        } else {
            parent::apply($source);
        }
    }
}