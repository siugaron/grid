<?php
namespace Spark\Grid\Filter;

use Spark\Grid\Filter;
use Spark\Grid\Source;
use Spark\Grid\Source\Builder;
use Spark\Grid\Source\Collection;
use Carbon\Carbon as Date;

// ToDo
class DateRange extends Filter {

    public function render() {
        $gridid = $this->getGrid()->getId();
        $name = $gridid . '[filters][' . $this->getName() . ']';

        $value = $this->getValue();
        if (isset($value['from']) && $value['from'] > 0) {
            $value['from'] = $value['from'];
        } else {
            $value['from'] = '';
        }
        if (isset($value['to']) && $value['to'] > 0) {
            $value['to'] = $value['to'];
        } else {
            $value['to'] = '';
        }

        $content = '<div class="field_notice row-no-padding">
            <div class="col-sm-12">
                <input class="form-control hasDatepicker"
                        data-provide="datepicker"
                        data-date-format="dd.mm.yyyy"
                        name="' . $name . '[from]"
                        type="text"
                        id="date"
                        value="'.$value['from'].'"></div>
            <br><br>
            <div class="col-sm-12">
                <input class="form-control hasDatepicker"
                        data-provide="datepicker"
                        data-date-format="dd.mm.yyyy"
                        name="' . $name . '[to]"
                        type="text"
                        value="'.$value['to'].'"
                        id="date"></div>
        </div>';

        return $content;
    }

    public function apply($source) {

        $this->initValue();
        $options = $this->getOptions();
        $value = $this->getValue();

        if ($value == '') {
            return true;
        }

        $data = $source->getData();

        if ($source instanceof Builder) {

            if (@$value['from'] > 0) {
                $data->where($this->getName(), ' >=', $value['from']);
            }
            if (@$value['to'] > 0) {
                $data->where($this->getName(), ' <=', $value['to']);
            }

        } elseif($source instanceof Collection) {
            $field_name = $this->getName();

            if(@$value['from'] > 0) {
                $data = $data->filter(function($row) use ($value, $field_name)
                {

                    $field = ($row->$field_name instanceof Date)? $row->$field_name->getTimestamp() : strtotime($row->$field_name);
                    $date = Date::createFromFormat('d.m.Y H:i:s', $value['from'] . " 00:00:00")->getTimestamp();

                    if ($field >= $date)
                        return true;

                });
            }

            if(@$value['to'] > 0) {
                $data = $data->filter(function($row) use ($value, $field_name)
                {
                    $field = ($row->$field_name instanceof Date)? $row->$field_name->getTimestamp() : strtotime($row->$field_name);
                    $date = Date::createFromFormat('d.m.Y H:i:s', $value['to'] . " 23:59:59")->getTimestamp();

                    if ($field <= $date)
                        return true;

                });
            }
        } else {
            throw new \Exception('Неизвестный тип источника данных', 503);
        }

        $source->setData($data);
    }
}