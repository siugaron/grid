<?php
/**
 * Источник данных на основе Builder
 *
 * Используется когда источник данных Illuminate\Database\Query\Builder
 */

namespace Spark\Grid\Source;

use Spark\Grid;
use Spark\Grid\Source;
use Illuminate\Pagination\Paginator;

/**
 * Class Builder
 * @package Spark\Grid\Source
 *
 * @property \Illuminate\Database\Query\Builder $_data
 */
class Builder extends Source
{
    public function order($column, $dir = "ASC")
    {
        $this->_sortColumn = $column;
        $this->_sortDirection = strtoupper($dir);
        
        $this->_data->orderBy($this->_sortColumn, $this->_sortDirection);
    }
    protected $_sortColumn;
    protected $_sortDirection;

    public function getSortColumn() {
        return $this->_sortColumn;
    }
    public function getSortDirection() {
        return $this->_sortDirection;
    }

    public function count()
    {
        return $this->_data->count();
    }

    public function isSortable() {
        return true;
    }

    public function initPaginatorAdapter($options = array())
    {
        $currentPage = $this->getGrid()->getPage();
        Paginator::currentPageResolver(function() use ($currentPage) {
            return $currentPage;
        });

        // Filters
        $filters = $this->getGrid()->getFilters();
        foreach ($filters as $filter) {
            $filter->apply($this);
        }

        return $this->_data->paginate($this->getGrid()->getPerPage());
    }

}