<?php
// ToDo

namespace Spark\Grid\Column;

use Spark\Grid\Column;

class Date extends Column {

    protected $_format = 'd.m.Y H:i:s';
    protected $_stripTimeIfNull = true;

    public function setFormat($value) {
        $this->_format = $value;
        $this->_stripTimeIfNull = false;
    }

    public function render($value)
    {
        if (empty($value)||$value==='0000-00-00 00:00:00'||$value=="-0001-11-30 00:00:00") {
            return '';
        }
        
        if (preg_match("/^\d{1,15}$/i", $value)) {
            // Timestamp, do nothing
        } else {
            $value = strtotime($value);
        }
        if ($this->_stripTimeIfNull && date("H:i:s", $value) == "00:00:00") {
            $this->_format = trim(str_replace("H:i:s", '', $this->_format));
        }

        return date($this->_format, $value);
    }
}