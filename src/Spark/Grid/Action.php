<?php
/**
 * Абстрактрый класс для Action - действия доступные для работы с элементами грида. Отображаются в углу области тайтла
 */
namespace Spark\Grid;

class Action
{   
    protected $_order;
    

    public function __construct($name, $options = array())
    {   
        $this->setName($name);
        $this->setOptions($options);
    }

    protected $_name;

    public function setName($name) {
        $this->_name = $name;
        return $this->_name;
    }

    public function getName() {
        return $this->_name;
    }

    protected $_options = array();

    public function setOptions(array $options)
    {
        foreach ($options as $name => $value) {
            $method = "set" . ucfirst($name);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        $this->_options = $options;
    }

    public function setOption($name, $value)
    {
        $this->_options[$name] = $value;
    }

    public function getOption($name = '')
    {
        if (isset($this->_options[$name])) {
            return $this->_options[$name];
        }
        return null;
    }
    public function appendOption ($name, $value) {
        @$this->_options[$name] .= $value;
        return $this;
    }
    
    
    /**
     * @var Spark\Grid
     */
    protected $_grid;

    /**
     * @param Spark\Grid $grid
     * @return void
     */
    public function setGrid(Grid $grid)
    {
        $this->_grid = $grid;
    }

    /**
     * @return Spark\Grid
     */
    public function getGrid()
    {
        return $this->_grid;
    }

    

    public function setOrder($order)
    {
        $this->_order = (int) $order;
        return $this;
    }

    public function getOrder()
    {
        return $this->_order;
    }

    protected $_label;

    public function setLabel($label = '')
    { 
        $this->_label = $label;
    }

    public function getLabel()
    {
        return $this->_label;
    }

    protected $_icon;

    protected function setIcon($icon = null) {
        $this->_icon = $icon;
        return $this->_icon;
    }

    protected function getIcon() {
        if($this->_icon==null||$this->_icon==false) {
            $this->_icon = $this->setIcon();
        }
        return $this->_icon;
    }


    protected function applyLink () {
        $link = $this->getOption("link");
        if (is_string($link)) {
            return $link;
        }

        $route = @$link["route"];
        $params = @$link["params"];
        $get = @$link["get"];

        if (!is_array($params)) {
            $params = array();
        }
        if (!is_array($get)) {
            $get = array();
        }
        foreach ($params as $param => $value) {
            $match = array();
            preg_match('/{(.*)}/iu', $value, $match);
            if (!isset($match[1])) {
                continue;
            }
        }
        foreach ($get as $param => $value) {
            $match = array();
            preg_match('/{(.*)}/iu', $value, $match);
            if (!isset($match[1])) {
                continue;
            }
        }

        
        $link = route($route, $params);

        if (sizeof($get) > 0) {
            $link .= "?";
            foreach ($get as $key => $val) {
                $link .= "&$key=" . urlencode($val);
            }
        } 
        return $link;
    }


    

    public function render()
    {
        $link = $this->applyLink();
        $options = $this->getOption("link");

        $label = $this->_label;
        $title = @$options["title"];
        $target = @$options["target"];

        // /* Проверяем переданы ли какие то GET параметры */
        // $query_string = "?" . $_SERVER['QUERY_STRING'];

        $class = @$options['class'];
        if (!empty($class)) {
            $class = ' class="' . $class . '"';
        }

        if($this->getIcon()!==null) {
            $content = '
                <a href="'.$link .'" class="" '. $title . $target . ' ><i class="fa fa-'.$this->_icon.'"></i> '.$label.'</a>
            ';
        } elseif(strlen($this->getLabel())>0) {
            $content = '<a href="' . $link . '"' . $class . ''. $title . $target . '>' . $label . '</a>';
        } else {
            $content = '<a href="' . $link . '"' . $class . ''. $title . $target . '>Ok</a>';
        }
        return $content;
    }

}