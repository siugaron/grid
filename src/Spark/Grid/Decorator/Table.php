<?php

namespace Spark\Grid\Decorator;

use Spark\Grid\Decorator;

class Table  extends Decorator
{
    public function render($content)
    {
        $order_url = $this->getGrid()->getOrderUrl();
        $class = $this->getGrid()->getClass();
        if(empty($class)) {
        	$class = '';
        }
        $content = '<div class="table-'.$class.'">
                        <table ' . (($order_url == null) ? '' : 'data-order-url="' . $order_url . '"') . ' class="table table-striped grid-content" id="grid-' . $this->getGrid()->getId() . '">' . $content . 
                        '</table>
                    </div>';
        return $content;
    }
}