<?php

namespace Spark\Grid;

use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Illuminate\Pagination\LengthAwarePaginator;
use Spark\Grid\Column;
use Spark\Grid\Source;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Grid
{

    protected $_itemsPerPage = 50;
    
    protected $_defaultSortColumn = null;
    protected $_defaultSortDir = 'ASC';
    
    protected $_showPerPageSelector = true;
    
    protected $_summationData = array();
   
    public $renderedRows = 0;

    protected $_actions = array();

  
    public function __construct($id, $data, $options = array())
    {
        $this->setId($id); 

        if (sizeof($options) > 0) {
            foreach ($options as $key => $value) {
                $method = "set" . ucfirst($key);
                if (method_exists($this, $method)) {
                    $this->$method($value);
                }
            }
        }
        $this->populateParams();

        $this->_initSource($data, $options);
        $this->_applySourceParams();

        $this->init();
        
        $this->_applyFilters();
        //$this->_initPaginator();

    } 

    public function init()
    {  


    }

    /**
     * @var string Идентификатор грида
     */
    protected $_id;

    /**
     * @param string $id
     * @return Spark\Grid
     */
    public function setId($id = '')
    {
        if (empty($id)) {
            $id = uniqid();
        }
        $this->_id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @var array Колонки грида
     */
    protected $_columns = array();


    public function createColumn($name, $type, $options = array()) {
        $type = ucfirst($type);
        $class = 'Spark\\Grid\\Column\\' . $type;
        $obj = new $class($name, $options);
        $obj->setGrid($this); 
        return $obj;
    }

    /**
     * @param string $name Имя колонки
     * @param string $type Тип колонки
     * @param array $options Опции колонки
     * @return Spark\Grid
     */
    public function addColumn($name, $type, $options = array())
    {
        $obj = $this->createColumn($name, $type, $options);
        /*if ($this->getSource()->getSortColumn() === $name) {
            $obj->setSort($this->getSource()->getSortDirection());
        }*/
        $this->_columns[$name] = $obj; 
        return $this;
    }

    public function removeColumn($name)
    {
        if (isset($this->_columns[$name])) {
            unset($this->_columns[$name]);
        }
        return $this;
    }

    public function getColumn($name)
    {
        if (isset($this->_columns[$name])) {
            return $this->_columns[$name];
        }
        return null;
    }

    public function getColumns()
    {   
        if(is_array($this->_columns)&&sizeof($this->_columns)==0) {
            $this->_columns = $this->sortArray($this->_columns);
        }
        return $this->_columns;
    }

   

    public function getCurrentRows()
    {
        throw new Exception("getCurrentRows is not implemented");
        // @ToDo return $this->getPaginator()->getCurrentItems();
    }

     // RowClassCallback
    
    protected $_rowClassCallback = null;
    
    public function setRowClassCallback($callback) {
        $this->_rowClassCallback = $callback;
        return $this;
    }
    
    public function getRowClassCallback() {
        return $this->_rowClassCallback;
    }

    // Global cell Callback
    /*
     * Replace cell renderer if return not null
     * 
     */
    
    protected $_cellCallback = null;
    
    public function setCellCallback($callback) {
        $this->_rowCellback = $callback;
        return $this;
    }
    
    public function getCellCallback() {
        return $this->_rowCellback;
    }
    
    protected $_filters = array();

    public function createFilter($name, $type, $options = array())
    {
        $type = ucfirst($type);
        $class = 'Spark\\Grid\\Filter\\' . $type;
        $filter = new $class($name, $options);
        $filter->setGrid($this);
        return $filter;
    }

    public function addFilter($name, $type, $options = array())
    {
        $filter = $this->createFilter($name, $type, $options);
        $this->_filters[$name] = $filter;
        return $this;
    }

    public function removeFilter($name = '')
    {
        if (isset($this->_filters[$name])) {
            unset($this->_filters[$name]);
        }
    }

    public function hasFilters() {
        if (sizeof($this->_filters) > 0) {
            return true;
        }
        return false;
    }

    public function getFilters() {
        if(is_array($this->_filters)) {
            $this->_filters = $this->sortArray($this->_filters);
        }
        return $this->_filters;
    }
    
    public function getFilter($name) {
        if (isset($this->_filters[$name])) {
            return $this->_filters[$name];
        }
    }

    /**
     * Применяет фильтры к исходным данным.
     * @return Spark\Grid
     */
    protected function _applyFilters()
    {
        $filters = $this->getFilters();
        foreach ($filters as $filter) {
            $filter->apply($this->getSource());
        }
        return $this;
    }


    /**
     * @var string
     */
    protected $_order_url = null;

    /**
     * @param string $url
     * @return Spark\Grid\Grid 
     */
    public function setOrderUrl($url)
    {
        if (isset($url) && is_string($url)) {
            $this->_order_url = $url;
        }
        return $this;
    }
    
    public function getOrderUrl() {
        return $this->_order_url;
    }

    /**
     * @var string
     */
    protected $_class = null;

    /**
     * @param string $class
     * @return Spark\Grid\Grid 
     */
    public function setClass($class)
    {
        if (isset($class) && is_string($class)) {
            $this->_class = $class;
        }
        return $this;
    }
    
    public function getClass() {
        return $this->_class;
    }

    /**
     * @var PaginatorContract
     */
    protected $_paginator = null;

    protected function _initPaginator()
    {
        if ($this->_paginator !== null) { return; }
        if($this->getSource()->initPaginatorAdapter() instanceof PaginatorContract) {
        	$this->_paginator = $this->getSource()->initPaginatorAdapter();
        } else {
        	$this->_paginator = new LengthAwarePaginator($this->getSource()->initPaginatorAdapter(), $this->getSource()->count(), $this->_itemsPerPage);
        }
    }
    

    protected $_params = null;

    public function getParam($name){
        $this->populateParams();
        if (isset($this->_params[$name])) {
            return $this->_params[$name];
        }
        return null;
    }

    protected function populateParams()
    {
        if ($this->_params != null) {
            return;
        }
        
        $this->_params = $params = \Input::get($this->getId());
    
        if (isset($params['perpage'])) {
            $this->setPerpage($params['perpage']);
        }
        if (isset($params['page'])) {
            $this->setPage($params['page']);
        }
    }

    protected $_page = 1;

    public function setPage($value) {
        $this->_page = $value;
        return $this;
    }

    public function getPage() {
        return $this->_page;
    }

    public function setPerpage($perPage) {
        $this->_itemsPerPage = $perPage;
    }

    public function getPerpage() {
        return $this->_itemsPerPage;
    }

    /**
     * @return Paginator
     */
    public function getPaginator()
    {
        $this->_initPaginator();
        return $this->_paginator;
    }


    /**
     * Returns if table have at least 1 row
     *
     * @return bool
     */
    public function hasRows() {
       if ($this->getPaginator()->getTotalItemCount() > 0) {
           return true;
       }
       return false;
    }

    /**
     * @var \Spark\Grid\Source
     */
    protected $_source;

    /**
     * Инициализирует источник данных
     *
     * @param mixed $data Данные
     *
     * @return $this
     */
    protected function _initSource($data, $options = array())
    {   
        
        if ($data instanceof Collection) {
            $sourceType = 'Spark\Grid\Source\Collection';
        } elseif($data instanceof Builder) {
            $sourceType = 'Spark\Grid\Source\Builder';
        }/* elseif($data instanceof HasMany) {
            $sourceType = 'Spark\Grid\Source\HasMany';
        } */else {
            throw new \Exception('Неизвестный тип данных');
        } 

        $this->_source = new $sourceType($data, $options); 
        $this->_source->setGrid($this);

        return $this->_source; 
    }


    protected function _applySourceParams()
    {
        $sorted = false;
        $sort = $this->getParam('sortby');
       
        if (!empty($sort)) {
            $sort = preg_replace('/[^A-Za-z0-9_-]+/', '', $sort); //sanitize input on column
            if ($this->_source->isSortable()) {
                $sortDir = $this->getParam('sortdir');
                if (strtoupper($sortDir) != 'DESC') {
                    $sortDir = 'ASC';
                } else {
                    $sortDir = 'DESC';
                }
                $this->_source->order($sort, $sortDir);
                //$this->getColumn($sort)->setSort($sortDir);
                $sorted = true;
            }
        }
        
        // default sort
        if (!$sorted && $this->_defaultSortColumn != null && $this->_source->isSortable()) {
            $this->_source->order($this->_defaultSortColumn, $this->_defaultSortDir);
        }
    }

    /**
     * @return Spark\Grid\Source\Source
     */
    public function getSource()
    {
        return $this->_source;
    }

    
    protected $_decorators = [];

    public function createDecorator($name, $options = array()) {
        $name = ucfirst($name);
        $class = 'Spark\\Grid\\Decorator\\' . $name;
        $obj = new $class();
        $obj->setGrid($this);
        return $obj;
    }

    public function addDecorator($name, $options = array())
    {
        $this->_decorators[$name] = $this->createDecorator($name, $options);
        return $this;
    }

    public function removeDecorator($name)
    {
        if ($this->getDecorator($name)) {
            unset($this->_decorators[$name]);
            return true;
        }
        return false;
    }
    
    public function getDecorators()
    {
        return $this->_decorators;
    }

    public function getDecorator($name)
    {
        if (isset($this->_decorators[$name])) {
            return $this->_decorators[$name];
        }
        return null;
    } 

    public function loadDefaultDecorators()
    {
        if (sizeof($this->_decorators) == 0) {
            $this->addDecorator("tableHeader");
            $this->addDecorator("body");
            $this->addDecorator("table");
            $this->addDecorator("batch");
            $this->addDecorator("title");
            $this->addDecorator("filters");
            $this->addDecorator("paginator");
            $this->addDecorator("panel");
        }
    } 

    public function render()
    {
        $content = '';
        $this->loadDefaultDecorators();
        foreach ($this->getDecorators() as $decorator) {
           $content = $decorator->render($content); 
        }
        return $content;
    }


    
    public function __get($name) {
        if (isset($this->_columns[$name])) {
            return $this->_columns[$name];
        }
        return null;
    }
    
    public function setSummationData($data) {
        $this->_summationData = $data;
    }
    
    public function getSummationData() {
        return $this->_summationData;
    }
    
    public function setShowPerPageSelector ($value = true) {
        $this->_showPerPageSelector = $value;
    }
    
    public function getShowPerPageSelector () {
        return $this->_showPerPageSelector;
    }
    
    
    protected $_batchActions = array();
    
    public function addBatchAction($name, $url) {
        $this->_batchActions[$name] = $url;
    }
    public function getBatchActions() {
        return $this->_batchActions;
    }  

    /**
     * Преобразование данных паджинатора в массив
     */

    public function loadData($toArray = false) {

        $data = $this->getPaginator();

        if (! is_array($data)) {
            if ($data instanceof PaginatorContract) {
                if($toArray) {
                    $data = $data->toArray();
                    $data = $data["data"];
                } else {
                    $data = $data->getCollection();
                }
                
            } else {
                $add = '';
                if (is_object($data)) {
                    $add = get_class($data);
                } else {
                    $add = '[no object]';
                }
                
                throw new \Exception('The paginator returned an unknow result: ' . $add . ' (allowed: \ArrayIterator or a plain php array)');
            }


        } 

        return $data; 
    }

    /**
     * Title грида
     * @param $title  - string
     */
    protected $_title;

    public function setTitle($title) {
        $this->_title = $title;
        return $this;
    }

    public function getTitle() {
        return $this->_title;
    }


    /**
     * Сортировка объектов по ордеру (используется для фильтров и колонок)
      * @param $arr - array
     */

    protected function sortArray($array) {
        if(is_array($array)) {
              uasort($array,function($f1,$f2){
                if(method_exists($f1, 'getOrder')&&method_exists($f2, 'getOrder')) {
                    if($f1->getOrder() < $f2->getOrder()) return -1;
                    elseif($f1->getOrder() > $f2->getOrder()) return 1;
                    else return 0;
                } else {
                    throw new \Exception("Метод getOrder не определен у объекта", 1);
                }
                
            });
        }
        return $array;
    }


    /**
    * Создание ссылки для изменения параметров вызова грида
    */

    public function link($params = array()) {
        $gridid = $this->getId();
        $query = $_GET;
        if (!isset($query[$gridid])) {
            $query[$gridid] = array();
        }
        foreach ($params as $key => $val) {
            $query[$gridid][$key] = $val;
        }
        $url = http_build_query($query);
        return '?' . $url;
    }

    /**
    * ToDo: Экшены управления из тайтла  грида
    **/

    /**
     * @param string $name
     * @param array $action
     *
     * @return $this
     */
    public function addAction($name, $action)
    {
        $newAction = new Action($name, $action);
        $newAction->setGrid($this);
        $this->_actions[$name] = $newAction;

        return $this;
    }

    public function removeActions() {
        $this->_actions = [];
        return $this;
    }

    public function getActions() {
        return $this->_actions;
    }

    protected $_panelLabel = '';  

    public function setPanelLabel($value) {
        $this->_panelLabel = $value;
        return true;
    }

    public function getPanelLabel() {
        return $this->_panelLabel;
    }

    protected $_panelControls = '';  

    public function setPanelControls($value) {
        $this->_panelControls = $value;
        return true;
    }

    public function getPanelControls() {
        return $this->_panelControls;
    }

    public function appendPanelControls($value) {
        $this->_panelControls .= $value;
        return true;
    }

}